/*Mirror mount
Code by Kerri Harrington, Bath Open INstrumentation group

Licence: Cern OHL

Design acknowledgements: Julian Stirling
*/

mount_width = 85;
mount_height = 85;
mount_t = 20/2; //thickness of mount
mount_thumbscrew_d = 13.1; //where mirror mount thumbscrews are from centre of mirror mount to thumbscrew centre
optic_diameter = 31.8; //directly behind mirror
motor_h_sep = 35; //separation of holes for motor screws 
motor_thumbscrew_diameter = 24/2; //diameter for the holes where the shaft couplers connect from motor to KM100 mirror mount thumbscrews
motor_hoffsetplacement = 8; //offset of holes for the stepper motors
motor_m4_hole = 3.6; //for tap
fudge = 0.5;


difference()
{
    cube([mount_width,mount_height,mount_t], true);
    //Hole for optic
    translate([0,0, -mount_t]){cylinder(mount_t*4, optic_diameter/2, optic_diameter/2,$fn=40);}
    //Hole for beam coupler of motor 1 - inline with mount thumbscrew
    translate([optic_diameter-mount_thumbscrew_d,optic_diameter-mount_thumbscrew_d,-mount_t]){cylinder(mount_t*3, motor_thumbscrew_diameter, motor_thumbscrew_diameter, $fn=40);}
    //Holes for stepper motor 1
    translate([optic_diameter-mount_thumbscrew_d-motor_h_sep/2,optic_diameter-mount_thumbscrew_d+motor_hoffsetplacement,-mount_t]){ cylinder(mount_t*3, motor_m4_hole, motor_m4_hole, $fn = 3);}
    translate([optic_diameter-mount_thumbscrew_d+motor_h_sep/2,optic_diameter-mount_thumbscrew_d+motor_hoffsetplacement,-mount_t]){ cylinder(mount_t*3, motor_m4_hole, motor_m4_hole, $fn = 3);}
    //Hole for beam coupler of motor 2 - inline with mount thumbscrew
    translate([-(optic_diameter-mount_thumbscrew_d),-(optic_diameter-mount_thumbscrew_d),-mount_t]){ cylinder(mount_t*3, motor_thumbscrew_diameter, motor_thumbscrew_diameter, $fn=40);}
    //Holes for stepper motor 2
    translate([-(optic_diameter-mount_thumbscrew_d-motor_h_sep/2),-(optic_diameter-mount_thumbscrew_d+motor_hoffsetplacement),-mount_t]){ cylinder(mount_t*3, motor_m4_hole, motor_m4_hole, $fn = 3);}
    translate([-(optic_diameter-mount_thumbscrew_d+motor_h_sep/2),-(optic_diameter-mount_thumbscrew_d+motor_hoffsetplacement),-mount_t]){ cylinder(mount_t*3, motor_m4_hole, motor_m4_hole, $fn = 3);}

    //Remove unwanted material
    translate([0,0,-mount_t]){ mirror([1,0,0]){ linear_extrude(height=4*mount_t) {polygon(points=[  [0-fudge,-mount_height/2-fudge], [-mount_width/2-fudge, -mount_height/2-fudge], [-mount_width/2-fudge, 0-fudge]  ]); }}}
    translate([0,0,-mount_t]){ mirror([0,1,0]){ linear_extrude(height=4*mount_t) {polygon(points=[  [0-fudge,-mount_height/2-fudge], [-mount_width/2-fudge, -mount_height/2-fudge], [-mount_width/2-fudge, 0-fudge]  ]); }}}

}