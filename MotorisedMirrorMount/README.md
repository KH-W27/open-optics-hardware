#Motorising a ThorLabs mirror mount

## Bill Of Materials

* 1 x 3D printed piece ('mirrormountformotors.scad')
* 2 x Ruland FCMR19-5-5A - Aluminium flexible beam coupling, (5mm & 5mm bore) 
* 2 x 28BYJ-48 Stepper motors
* 2 x ThorLabs F25ST100 - 1" 1/4-80 adjuster screws
* 1 x ThorLabs KM100 (or KM100CP/M) -  mirror mount

## Modifying the shaft couplers

## PCB
The mirror mount is controlled using a SANGAboard v0.2 
[PCB files can be found here](https://github.com/rwb27/openflexure_nano_motor_controller/tree/master/pcb_design)

## Assembly

**TODO**


# Software for mirror mount

**TODO**

## Arduino firmware for Nano on Sangaboard
Flash the Arduino firmware onto the Sangaboard using the Arduino IDE, the firmware is available [here](https://github.com/rwb27/openflexure_nano_motor_controller/tree/master/arduino_code)

## Installing the python package

Clone or download the [OpenFlexure nano motor controller repository](https://github.com/rwb27/openflexure_nano_motor_controller)

Navigate in a terminal to the downloaded repository and run:

```
pip install -e .
```

## Controlling the mirror mount

The simple example below shows how to move the motors for the mirror mount

```python
from openflexure_stage import OpenFlexureStage as OFS
import time
# Change /dev/ttyUSB0' to the address of your Arduino. Perhaps COM3 on windows!?
mount = OFS('/dev/ttyUSB0')
mount.move_rel(1000,'y')
time.sleep(1)
mount.move_rel(1000,'z')
time.sleep(1)
mount.move_rel(-1000,'y')
time.sleep(1)
mount.move_rel(-1000,'z')
```