
![](logo.png) 

## Open Optics Hardware
This is a space for simple open hardware to replace expensive hardware using in an optics lab. The idea is that some components will be useful because they are cheaper, others because they are customisable.