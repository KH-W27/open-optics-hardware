/*Beam Height Gauge
Code by Julian Stirling, Bath Open INstrumentation group

Licence: Cern OHL

Design acknowledgements: Charlotte Parry & Kerri Harrington
*/

/*Parameters to change*/
beamHeight=75;
heightAbove=25;
width = 40;
thickness = 10;
baseThickness= 10;
baseWidth = 40;
nomTapD= 4;
holeSep = 25;
//Measured magnet diameter
magDiam=6;
//thickness at the bottom of the magnet tray 
magTrayThickness= 2;

/*Calculcations*/
totalHeight = beamHeight + heightAbove;
//Ratio of inscribed to excribed circle for a equilateral triagle
//This is used to make triangular holes with an inscribed  circle equal to the tap diameter
triangleInsExs = 2;
holeSize=nomTapD*triangleInsExs;
holeYPos = (baseWidth-thickness-nomTapD)/2;
trayYPos = -(baseWidth-thickness-magDiam)/2;
trayDiam = magDiam+1;
fudge=.01;
/*Build*/

difference(){
    union()
    {
        translate([0,0,totalHeight/2]){ cube([width,thickness,totalHeight],center=true); }
        translate([0,0,baseThickness/2]){ cube([width,baseWidth,baseThickness],center=true); }
    }
    translate([holeSep/2,holeYPos,baseThickness/2]){cylinder(2*baseThickness,d1=holeSize,d2=holeSize,center=true,$fn=3);}
    translate([-holeSep/2,holeYPos,baseThickness/2]){cylinder(2*baseThickness,d1=holeSize,d2=holeSize,center=true,$fn=3);}
    translate([holeSep/2,trayYPos,thickness/2+magTrayThickness]){cylinder(thickness,d1=trayDiam,d2=trayDiam,center=true,$fn=32);}
    translate([-holeSep/2,trayYPos,thickness/2+magTrayThickness]){cylinder(thickness,d1=trayDiam,d2=trayDiam,center=true,$fn=32);}
    translate([0,-(thickness/2-1+fudge),3*beamHeight/4]){rotate([90,0,0]){OOHlogo(1);}}
    translate([0,0,beamHeight])
    {
        union()
        {
            rotate([90,0,0]){crossCone();}
            rotate([-90,0,0]){crossCone();}
        }
    }
}

module OOHlogo(height)
{
    linear_extrude(height) translate([-17.5,-16]) resize([35,0],auto=true) import("logo.dxf");
}

module crossCone()
{
    difference(){
        cylinder(thickness,r1=0.01,r2=thickness*tan(59));
        for (i = [0:3])
        {
            rotate([0,0,i*90]){rotate([0,-59,0]){translate([0,0,2]){cylinder(2*thickness,r1=.8,r2=.8,$fn=3);}}}
        }
    }
}